@extends('layouts.app')

@section('content')
<div class="ui grid">
    <div class="sixteen wide column">
        <div class="ui comments">
            <div class="comment">
                <a class="avatar">
                    <img src="{{url('/uploads/avatars/'.$perfil->avatar)}}">
                </a>
                <div class="content">
                    <div class="metadata">
                        <div class="date">{{$post->created_at->format('d/m/Y')}}</div>
                        <div class="rating">
                            <i class="star icon"></i>
                            5 Faves
                        </div>
                    </div>
                    <div class="text">
                        <p><b>{{$post->title}}</b></p>
                        {!! $post->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection