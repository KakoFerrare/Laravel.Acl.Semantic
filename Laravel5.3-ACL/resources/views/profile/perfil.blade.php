@extends('template.template')

@section('content')

<div class="ui grid">
    <h3>{{$user->name}} Perfil</h3>
    <div class="sixteen wide column">
        @if( Session::has('success') )

        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                Perfil Atualizado.
            </div>
            <p>
                {{Session::get('success')}}
            </p>
        </div>

        @endif
    </div>


    <div class="five wide column">
        <img id="avatar-img" class="ui massive rounded image" src="{{url('/uploads/avatars/'.$perfil->avatar)}}">
        @if ($errors->has('avatar'))
        <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('avatar')}}</div>
        @endif
    </div>

    <div class="eleven wide column">

        {{Form::open(array('url' => '/profile', 'files' => true,'class'=>'ui form'))}}
        {{ csrf_field() }}
        <div class="field">
            <input type="button" class="large ui fluid button" id="btn-avatar" value="Escolher Imagem"/>
            <input type="file" name="avatar" id="avatar" style="display: none" accept="image/*">
            <div class="ui bottom attached progress" id="progress-img">
                <div class="bar">

                </div>
            </div>

        </div>

        <div class="field">
            {{Form::label('name')}}
            {{ Form::text('name',$user->name, ['disabled'=>'disabled'])}}
        </div>

        <div class="field ui calendar" id="div-nascimento">
            {{Form::label('nascimento')}}
            <div class="ui input left icon">
                <i class="calendar icon"></i>
                {{ Form::text('nascimento',  isset($perfil->nasc) ? date('d/m/Y', strtotime($perfil->nasc)) : null ,['data-mask'=>'00/00/0000'])}}
            </div>
        </div>

        <div class="field">
            {{Form::label('Sexo')}}
            <div class="grouped fields">
                <div class="ui radio checkbox {{$perfil->sex == 0 ? 'checked':''}}">
                    {{Form::radio('sexo', 0, null,['class'=>'hidden', $perfil->sex == 0 ?'checked':''])}}
                    {{Form::label('Feminino')}}
                </div>
            </div>
            <div class="grouped fields">
                <div class="ui radio checkbox {{$perfil->sex == 1 ? 'checked':''}}">
                    {{Form::radio('sexo', 1, null,['class'=>'hidden', $perfil->sex == 1 ?'checked':''])}}
                    {{Form::label('Masculino')}}
                </div>
            </div>
        </div>


        <div class="field">
            {{Form::label('telefone')}}
            {{Form::text('telefone', $perfil->tel, ['data-mask'=>'(00)0000-0000'])}}
        </div>

        <div class="field">
            {{Form::label('celular')}}
            {{Form::text('celular', $perfil->cel)}}
        </div>

        <div class="field">
            {{Form::label('Estado Civil')}}
            {{ Form::select('estado_civil',['' => '- Selecione um estado civil -'] + $status, $perfil->material_status_id, ['id'=>'status_civil']) }}
        </div>

        <div class="field">
            {{Form::label('Estado')}}
            {{ Form::select('estado', ['' => '- Selecione um estado -'] + $estados, null, ['id'=>'estado']) }}
        </div>

        <div class="field">
            {{Form::label('Cidade')}}
            <div class="ui left icon input loading">
                {{ Form::select('cidade', isset($cidade)? [$cidade->id => $cidade->name] : ['' => '- Selecione uma cidade -'], $perfil->city_id, ['id'=>'cidade', isset($cidade)? '' : 'disabled']) }}
                <i class="search icon" style="display: none"></i>
            </div>
        </div>

        <div class="field">
            {{Form::label('Endereço')}}
            {{Form::text('logradouro', $perfil->endereco, ['class' =>'ui sixteen input', 'placeholder'=>'Rua Fulano 333'])}}
        </div>


        <div class="field">
            {{Form::submit('Salvar',['class'=>'positive ui button'])}}
            {{Form::close()}}
        </div>
    </div>
</div>

<script>



    var options = {
        onKeyPress: function (val, event, currentField, options) {
            var masks = ['(00)00000-0000', '(00)0000-00000'];
            mask = (val.length < 14) ? masks[1] : masks[0]
            $('#celular').mask(mask, options);
        }
    };


    $(document).ready(function () {
        $.applyDataMask();
        $('#celular').mask('(00)00000-0000', options);
        $('#progress-img').progress({percent: 0});
        $('#div-nascimento').calendar({
            type: 'date',
            formatter: {
                date: function (date, settings) {
                    if (!date)
                        return '';
                    var day = date.getDate();
                    day = day <= 9 ? "0" + day : day;
                    var month = date.getMonth() + 1;
                    month = month <= 9 ? "0" + month : month;
                    var year = date.getFullYear();
                    return day + '/' + month + '/' + year;
                }
            }
        });
    });


    jQuery("#estado").change(function () {
        var id_estado = jQuery(this).val();
        $.ajax({
            url: 'cidade/' + id_estado,
            type: 'GET',
            dataType: 'JSON',
            beforeSend: function () {
                $(this).children('.loading').show();
            },
            success: function (data) {
                $('#cidade').empty();
                $('#cidade').removeAttr("disabled");
                jQuery.each(data, function (key, value) {
                    jQuery("#cidade").append('<option value="' + value.id + '">' + value.name + '</option>');
                });
            },
            fail: function () {
                $('#cidade').attr("disabled", "disabled");
            },
            done: function () {
                $(this).children('.loading').hide();
            }
        });
    });

    document.getElementById('btn-avatar').onclick = function () {
        document.getElementById('avatar').click();
    };
    document.getElementById("avatar").onchange = function () {
        var reader = new FileReader();
        reader.onloadstart = function (e) {
            if (e.total > 5000000) {
                reader.onabort();
            }
        }
        reader.onprogress = function (e) {
            if (e.lengthComputable) {
                var progress = (e.loaded * 100) / e.total;
                $('#progress-img').progress({percent: progress});
            }
        };
        reader.onload = function (e) {
            document.getElementById("avatar-img").src = e.target.result;
            $('#progress-img').progress({percent: 100});
        };
        reader.onabort = function () {
            reader.abort();
        };
        reader.readAsDataURL(this.files[0]);
    };


</script>
@endsection