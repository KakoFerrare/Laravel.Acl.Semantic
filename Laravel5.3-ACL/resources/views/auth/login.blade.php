@extends('layouts.app')

@section('content')

<div class="ui stackable grid container">
    <div class="sixteen wide column"><h2>Login</h2></div>
    <div class="eight wide centered column">
        <form class="ui large form" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

            <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                <div class="ui left icon input">
                    <input name="email" placeholder="E-mail" type="text" value="{{ old('email') }}">
                    <i class="user icon"></i>
                </div>
                @if ($errors->has('email'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('email') }}</div>
                @endif

            </div>
            <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                <div class="ui left icon input">
                    <input name="password" placeholder="Senha" type="password" >
                    <i class="lock icon"></i>
                </div>
                @if ($errors->has('password'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('password') }}</div>
                @endif
            </div>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" class="hidden" name="remember" {{ old('remember') ? 'checked' : ''}}>
                           <label>Lembrar-me</label>
                </div>
            </div>
            <button class="ui button" type="submit">Entrar</button>
            <a href="{{ url('/password/reset') }}">
                Esqueceu sua crendencial?
            </a>
        </form>
    </div>
</div>

<script>

    $('.ui.form')
            .form({
                fields: {
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'Insira um E-mail válido'
                            }
                        ]
                    },
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Insira a senha'
                            }
                        ]
                    }
                },
                inline: true,
                on: 'blur'
            });


</script>
@endsection
