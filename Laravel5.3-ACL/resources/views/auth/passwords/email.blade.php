@extends('layouts.app')

<!-- Main Content -->
@section('content')


<div class="ui stackable grid container">
    <div class="sixteen wide column"><h2>Resetar Senha</h2></div>
    <div class="eight wide centered column">
        @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                E-mail enviado com sucesso.
            </div>
            <p>{{ session('status') }}</p>
        </div>
        @endif
        <form class="ui large form" role="form" method="POST" action="{{ url('/password/email') }}">
            {{ csrf_field() }}

            <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                <label>E-mail Registrado</label>
                <input name="email" placeholder="E-mail" type="text" value="{{ old('email') }}">
                @if ($errors->has('email'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('email') }}</div>
                @endif

            </div>

            <button class="ui button" type="submit">Enviar Link</button>

        </form>
    </div>
</div>


<script>
    
    $('.message .close').on('click', function (){
       $(this).closest('.message').transition('fade'); 
    });

    $('.ui.form')
            .form({
                fields: {
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'email',
                                prompt: 'Insira um E-mail válido'
                            }
                        ]
                    }
                },
                inline: true,
                on: 'blur'
            });


</script>
@endsection
