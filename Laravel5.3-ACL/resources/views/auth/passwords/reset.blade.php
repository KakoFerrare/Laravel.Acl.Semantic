@extends('layouts.app')

@section('content')


<div class="ui stackable grid container">
    <div class="sixteen wide column"><h2>Recadastrar Senha</h2></div>
    <div class="eight wide centered column">

        <form class="ui large form" role="form" method="POST" action="{{ url('/password/reset') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">


            <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                <label>E-mail</label>
                <input name="email" placeholder="E-mail" type="text" value="{{ old('email') }}">
                @if ($errors->has('email'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                <label>Senha</label>
                <input name="password" placeholder="Senha" type="password" >
                @if ($errors->has('password'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('password') }}</div>
                @endif
            </div>


            <div class="field{{ $errors->has('password_confirmation') ? ' error' : '' }}">
                <label>Confirmação de Senha</label>
                <input id="password_confirm" name="password_confirmation" placeholder="Confirmar Senha" type="password" >
                @if ($errors->has('password_confirmation'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('password_confirmation') }}</div>
                @endif
            </div>
            
            <button class="ui button" type="submit">Resetar</button>

        </form>

    </div>
</div>

<script>
    $('.ui.form')
            .form({
                fields: {
                   email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O campo e-mail é obrigatório'
                            },
                            {
                                type: 'email',
                                prompt: 'Insira um E-mail válido'
                            }
                        ]
                    },
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O campo senha é obrigatório.'
                            }
                        ]
                    },
                    match: {
                        identifier: 'password_confirmation',
                        rules: [
                            {
                                type: 'match[password]',
                                prompt: 'A confirmação não confere com a senha.'
                            }
                        ]
                    }
                },
                inline: true,
                on: 'blur'
            });


</script>
@endsection
