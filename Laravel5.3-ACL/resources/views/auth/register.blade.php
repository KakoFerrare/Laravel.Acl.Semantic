@extends('layouts.app')

@section('content')

<div class="ui stackable grid container">
    <div class="sixteen wide column"><h2>Cadastrar-se</h2></div>
    <div class="eight wide centered column">

        <form class="ui large form" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
            
            <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                <label>Nome</label>
                <input name="name" placeholder="Nome" type="text" value="{{ old('name') }}">
                @if ($errors->has('name'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('name') }}</div>
                @endif
            </div>

            <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                <label>E-mail</label>
                <input name="email" placeholder="E-mail" type="text" value="{{ old('email') }}">
                @if ($errors->has('email'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                <label>Senha</label>
                <input name="password" placeholder="Senha" type="password" >
                @if ($errors->has('password'))
                <div class="ui basic red pointing prompt label transition visible">{{ $errors->first('password') }}</div>
                @endif
            </div>

            <div class="field">
                <label>Confirmação de Senha</label>
                <input id="password_confirm" name="password_confirmation" placeholder="Confirmar Senha" type="password" >
            </div>

            <button class="ui button" type="submit">Cadastrar</button>

        </form>

    </div>
</div>

<script>
    $('.ui.form')
            .form({
                fields: {
                    name: {
                        identifier: 'name',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O campo nome é obrigatório'
                            }
                        ]
                    },
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O campo e-mail é obrigatório'
                            },
                            {
                                type: 'email',
                                prompt: 'Insira um E-mail válido'
                            }
                        ]
                    },
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'O campo senha é obrigatório.'
                            }
                        ]
                    },
                    match: {
                        identifier: 'password_confirmation',
                        rules: [
                            {
                                type: 'match[password]',
                                prompt: 'A confirmação não confere com a senha.'
                            }
                        ]
                    }
                },
                inline: true,
                on: 'blur'
            });


</script>
@endsection
