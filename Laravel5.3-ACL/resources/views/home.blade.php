@extends('template.template')

@section('content')

<div class="ui equal width grid">
    <div class="column">
        <form action="/home" class="ui form" method="post">
            {!! csrf_field() !!}
            <div class="field">
                <input type="text" placeholder="Título do Post" name="title">
            </div>
            <div class="field">
                <textarea name="post"></textarea>
            </div>
            <div class="field">
                <input type="submit" class="positive ui button" name="enviar" value="Enviar">
            </div>
        </form>
        <hr>
        <h4>Posts</h4>
        @forelse($posts as $post)
        <div class="ui grid">
            <div class="sixteen wide column">
                <div class="ui comments">
                    <div class="comment">
                        <a class="avatar">
                            <img src="{{url('/uploads/avatars/'.$perfil->avatar)}}">
                        </a>
                        <div class="content">
                            <a class="author">{{$post->user->name}}</a>

                            <div class="metadata">
                                @if(isset($post->created_at))
                                <div class="date">{{$post->created_at->format('d/m/Y')}}</div>
                                @endif
                                <div class="rating">
                                    <i class="star icon"></i>
                                    5 Faves
                                </div>
                            </div>
                            <div class="text">
                                <p><b>{{$post->title}}</b>
                                    @can('update-post', $post)
                                    <a href="{{url("/post/$post->id/update")}}">Editar</a>
                                    @endcan
                                </p>
                                {!! $post->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <p>Nenhum Post</p>
        @endforelse
    </div>
</div>
<script>tinymce.init({selector: 'textarea'});</script>
@endsection
