<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="{{asset('css/semantic.min.css')}}"  media="screen,projection"/>
        <!-- Scripts -->
        <script type="text/javascript" src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/semantic.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/main.js')}}"></script>

        <script>
window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
        </script>
        
<style>
    .ui.grid.container{
        margin-top:4em;
    }
</style>
    </head>
    <body>

        <div id="app">
            <div class="ui secondary pointing menu">
                <a class="item" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <a class="item {{ Request::is('home') ?  "blue active" : ""}}" href="{{ url('/home') }}">
                    <i class="home icon"></i>home
                </a>
                <div class="right menu">

                    @if (Auth::guest())
                    <div class="item {{ Request::is('login') ?  "blue active" : ""}}">
                        <a href="{{ url('/login') }}">Login</a>
                    </div>
                    <div class="item {{ Request::is('register') ?  "blue active" : ""}}">
                        <a href="{{ url('/register') }}">Cadastrar</a>
                    </div>
                    @else
                    <div class="ui dropdown item">
                        {{ Auth::user()->name }} <i class="dropdown icon"></i>
                        <div class="menu">
                            <a class="item" href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="ui grid container">
            @yield('content')
        </div>

        <script>
            $('.ui.dropdown').dropdown();
            $('.ui.checkbox').checkbox();
        </script>

    </body>
</html>
