<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="{{asset('css/semantic.min.css')}}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{asset('css/calendar.css')}}"  media="screen,projection"/>
        <!-- Scripts -->
        <script type="text/javascript" src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/semantic.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/calendar.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.mask.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/tinymce/tinymce.min.js')}}"></script>
        <script>
window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
        </script>

        <style>
            .ui.grid.container{
                margin-top:4em;
            }
            .ui.footer.form-page { /* Increased specificity for SO snippet priority */
                position: absolute;
                bottom: 0;
                width: 100%;
                height: 50px;
                background: #DEDEDE;
            }

            .push {
                margin-bottom: 150px;
            }
        </style>
    </head>
    <body>
        <div id="app">
            <div class="ui secondary pointing menu">
                <a class="item" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <a class="item {{ Request::is('home') ?  "blue active" : ""}}" href="{{ url('/home') }}">
                    <i class="home icon"></i>home
                </a>
                <div class="right menu">

                    @if (Auth::guest())
                    <div class="item {{ Request::is('login') ?  "blue active" : ""}}">
                        <a href="{{ url('/login') }}">Login</a>
                    </div>
                    <div class="item {{ Request::is('register') ?  "blue active" : ""}}">
                        <a href="{{ url('/register') }}">Cadastrar</a>
                    </div>
                    @else
                    <div class="ui dropdown item">
                        {{ Auth::user()->name }} <i class="dropdown icon"></i>
                        <div class="menu">
                            <a class="item" href="{{ url('/profile  ') }}">
                                Perfil
                            </a>

                            <a class="item" href="{{ url('/logout') }}"   onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                Sair
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>

                    <div class="item">
                        <a href="#">
                            <i class="sidebar icon"></i>
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="ui container">
            <div class="ui wide right sidebar inverted vertical menu">
                <a class="item" href="{{url('/profile')}}">
                    <div class="five wide column">
                        <img class="ui centered large circular image" src="{{url('/uploads/avatars/'.$perfil->avatar)}}">
                    </div>
                </a>
                <a class="item"></a>
                <a class="item"></a>
            </div>
            <div class="push">
                <br>
                @yield('content')
            </div>
        </div>
        <div class="ui vertical footer segment form-page">
            <div class="ui container">
                <b>Todos os direitos reservados</b>
            </div>
        </div>
        <script>
            $('.ui.dropdown').dropdown();
            $('.ui.radio.checkbox').checkbox();
            $('.ui.sidebar').sidebar('setting', 'transition', 'overlay').sidebar('attach events', '.sidebar.icon');
            $('.sidebar.icon').removeClass('disabled');
            $('.ui.dropdown').dropdown();
            $('.ui.checkbox').checkbox();
            $('.message .close').on('click', function () {
                $(this).closest('.message').transition('fade');
            });
        </script>

    </body>
</html>
