<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Gate;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        //$posts = Post::get();
        $user = auth()->user();
        $perfil = $user->find($user->id)->profile;
        $posts = Post::all();
        //$posts = Post::where('user_id', auth()->user()->id)->get();
        return view('home', compact('posts', 'perfil'));
    }

    public function profile() {
        $user = auth()->user();
        $perfil = $user->find($user->id)->profile;
        $status = \App\Status::orderBy('name')->pluck('name', 'id')->all();
        $estados = \App\State::orderBy('name')->pluck('name', 'id')->all();
        $cidade = \App\City::find($perfil->city_id);
        //$profile = auth()->user()->with('profile');
        //dd($profile);
        //$posts = Post::where('user_id', auth()->user()->id)->get();
        return view('profile.perfil', compact('perfil', 'user', 'estados', 'status', 'cidade'));
    }

    public function update($idPost) {
        $user = auth()->user();
        $perfil = $user->find($user->id)->profile;
        $post = Post::find($idPost);
        //if(Gate::denies('update-post', $post))
        //   abort(403, 'Não Autorizado');

        $this->authorize('update-post', $post);
        return view('update-post', compact('post', 'perfil'));
    }
    
    
    public function createPost(Request $request){
        $post = Post::create(['title'=>$request['title'],
                              'description' => $request['post'],
                                'user_id'=> auth()->user()->id]);
        
        return redirect('/home');
    }

    public function rolesPermissions() {
        return dd(auth()->user()->name);
    }

}
