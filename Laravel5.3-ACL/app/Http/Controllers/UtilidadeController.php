<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Excel;
use Mail;
use Crypt;
use Hash;

class UtilidadeController extends Controller {

    public function gerarPdf() {
        $pdf = App::make('dompdf.wrapper');
        $users = \App\User::get();
        //$pdf->loadHTML('<h1>Test</h1>');
        $pdf->loadHTML(view('utilidade.pdf.pdf', compact('users')));
        return $pdf->stream();
        //return view('utilidade.pdf.pdf');
    }

    public function gerarExcel() {
        Excel::create('Nome Arquivo', function($excel) {
            $excel->sheet('Sheetname', function($sheet) {
                $array = [1, 2, 3, 4, 5];
                $sheet->loadView('utilidade.excel.excel', compact('array'));
            });
        })->export('xls');
        //OPCIONAL, SE CASO QUISER VISUALIZAR A VIEW
        return view('utilidade.excel.excel');
    }

    public function gerarMail() {
        Mail::send('utilidade.mail.mail',['variavel' =>'123'], function ($mail){
            $mail->from('kako@ferrare.com.br', 'Eu Mesmo');
            $mail->to('kakoferrare@hotmail.com', 'Kako')->
                    subject('Teste de envio de email por uma View');
            $mail->cc('kakoferrare87@gmail.com');
        });
    }
   
    public function gerarMailAnexo() {
        $to = [ 'email' => 'kakoferrare@hotmail.com', 'name' =>   'Kako'];
        Mail::send('utilidade.mail.mail',['variavel' =>'123'], function ($mail) use ($to){
            $mail->from('kako@ferrare.com.br', 'Eu Mesmo');
            $mail->to($to['email'], $to['name'])->
                    subject('Teste de envio de email por uma View com anexo')->
                    bcc('kakoferrare87@gmail.com')->
                    attach(storage_path('app/public/imgs/dog.jpg'));
        });
    }
    
    public function gerarCriptografia(){
        $val = '123';
        
        //MESMA COISA
        /*\Illuminate\Support\Facades\Crypt::*/
        echo $val.'<br><hr>';
        
        var_dump(Crypt::encrypt($val));
        $crypt = Crypt::encrypt($val);
        echo '<br><hr>';
        
        var_dump(Crypt::decrypt($crypt));
        echo '<br><hr>';
        
        $hash = Hash::make($val);
        var_dump($hash);
        echo'<br>';
        var_dump(Hash::check($val, $hash));
        echo'<br>';
        var_dump(Hash::check('12', $hash));
        echo '<br><hr>';
        
        var_dump(bcrypt($val));
        echo '<br><hr>';
        
        }

}
