<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class CityController extends Controller {

    private $state;

    public function __construct(State $state) {
        $this->state = $state;
    }

    public function getCitys($city_id) {
        $estado = $this->state->find($city_id);
        $cidade = $estado->city;
        return response()->json($cidade);
    }

}
