<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Image;
use Session;

class ProfileController extends Controller {

//    public function avatar(){
//        $image = Request::input('avatar');
//        return response()->json(['outro'=>'teste']);
//    }

    public function profile(Request $request) {
        $profile = new Profile();
        $data = str_replace("/", "-", $request['nascimento']);
        $data = date('Y-m-d', strtotime($data));
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $rules = [
                'avatar' => 'image|mimes:jpg,png,bmp,jpeg|max:5000'
            ];
            $validator = validator($request->all(), $rules);
            if ($validator->fails()) {
                return redirect('profile')->withErrors($validator);
            }
            //$nameFile = auth()->user()->name . '_avatar' . substr($file->getClientOriginalName(), -4);
            $nameFile = $file->getClientOriginalName();
            $path = public_path('uploads/avatars/');
            
            //$upload = $file->move($path, $nameFile);
            $upload = Image::make($file)->resize(300, 300)->save($path.$nameFile);
            
            $profile->where('user_id', auth()->user()->id)->update([
                'nasc' => $data,
                'avatar' => isset($nameFile) ? $nameFile : 'default.png',
                'sex' => $request['sexo'],
                'tel' => $request['telefone'],
                'cel' => $request['celular'],
                'material_status_id' => $request['estado_civil'],
                'city_id' => $request['cidade'],
                'endereco' => $request['logradouro']
            ]);
        } else {

            $profile->where('user_id', auth()->user()->id)->update([
                'nasc' => $data,
                'sex' => $request['sexo'],
                'tel' => $request['telefone'],
                'cel' => $request['celular'],
                'material_status_id' => $request['estado_civil'],
                'city_id' => $request['cidade'],
                'endereco' => $request['logradouro']
            ]);
        }

        $user = auth()->user();
        $perfil = $user->find($user->id)->profile;
        $status = \App\Status::orderBy('name')->pluck('name', 'id')->all();
        $estados = \App\State::orderBy('name')->pluck('name', 'id')->all();
        $cidade = \App\City::find($perfil->city_id);
        
        //Sessão temporária, 2 parametros são necessários
        //Nome da sessão e o Valor 
        //Lembrar de dar Use na Facade Session
        Session::flash('success', 'Perfil cadastrado com sucesso.');
        //return redirect('profile.perfil');
        return view('profile.perfil', compact('perfil', 'user', 'estados', 'status', 'cidade'));
    }

    public function citys($id) {
        $cidade = \App\City::where('state_id', $id)->get();
        return response()->json($cidade);
    }
    

}
