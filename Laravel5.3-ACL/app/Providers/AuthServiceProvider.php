<?php

namespace App\Providers;

use App\Post;
use App\User;
use App\Permission;
use App\Policies\PostPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //Post::class => PostPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Gate::define('update-post', function ($user, $post){
            return $user->id == $post->user_id;
        });
        
        /*
    $permissions = Permission::with('roles')->get();
    foreach ($permissions as $permission){
        Gate::define($permission->name, function (User $user) use ($permission){
            return $user->hasPermission($permission);
            });
        }

        Gate::before(function(User $user){
            if($user->hasAnyRoles('Adm'))
                return true;

        });
*/
    }
}
