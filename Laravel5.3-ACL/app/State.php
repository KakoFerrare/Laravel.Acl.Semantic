<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * Relationship one to many
     */
    public function city(){
        return $this->hasMany('App\City');
    }
}
