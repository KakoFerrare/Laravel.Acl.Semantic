<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['avatar','user_id','city_id'];
    
    public $rules = [
        'user_id'=>'required|integer',
        'avatar'=>'required|image|mimes:jpg,png',
        'nascimento'=>'required|date',
        'sex'=>'required|integer',
        'material_status_id'=>'required|integer',
        'tel'=>'required|max:20',
        'cel'=>'required|max:20',
        'city_id'=>'required|integer',
        'endereco'=>'required|max:200',
    ];
    
    public function user(){
        return $this->belongsTo(\App\User::class);
    }
    
    public function city(){
        return $this->hasOne(\App\City::class);
    }
}
