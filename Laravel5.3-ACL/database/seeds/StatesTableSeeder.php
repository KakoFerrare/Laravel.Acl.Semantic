<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $now = date("Y-m-d H:i:s");
        DB::table("states")->insert([
            [
                "id" => 11,
                "name" => "Rondônia",
                "abrv" => "RO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 12,
                "name" => "Acre",
                "abrv" => "AC",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 13,
                "name" => "Amazonas",
                "abrv" => "AM",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 14,
                "name" => "Roraima",
                "abrv" => "RR",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 15,
                "name" => "Pará",
                "abrv" => "PA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 16,
                "name" => "Amapá",
                "abrv" => "AP",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 17,
                "name" => "Tocantins",
                "abrv" => "TO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 21,
                "name" => "Maranhão",
                "abrv" => "MA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 22,
                "name" => "Piauí",
                "abrv" => "PI",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 23,
                "name" => "Ceará",
                "abrv" => "CE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 24,
                "name" => "Rio Grande do Norte",
                "abrv" => "RN",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 25,
                "name" => "Paraíba",
                "abrv" => "PB",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 26,
                "name" => "Pernambuco",
                "abrv" => "PE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 27,
                "name" => "Alagoas",
                "abrv" => "AL",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 28,
                "name" => "Sergipe",
                "abrv" => "SE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 29,
                "name" => "Bahia",
                "abrv" => "BA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 31,
                "name" => "Minas Gerais",
                "abrv" => "MG",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 32,
                "name" => "Espírito Santo",
                "abrv" => "ES",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 33,
                "name" => "Rio de Janeiro",
                "abrv" => "RJ",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 35,
                "name" => "São Paulo",
                "abrv" => "SP",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 41,
                "name" => "Paraná",
                "abrv" => "PR",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 42,
                "name" => "Santa Catarina",
                "abrv" => "SC",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 43,
                "name" => "Rio Grande do Sul",
                "abrv" => "RS",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 50,
                "name" => "Mato Grosso do Sul",
                "abrv" => "MS",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 51,
                "name" => "Mato Grosso",
                "abrv" => "MT",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 52,
                "name" => "Goiás",
                "abrv" => "GO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 53,
                "name" => "Distrito Federal",
                "abrv" => "DF",
                "created_at" => $now,
                "updated_at" => $now,
            ],
        ]);
    }

}