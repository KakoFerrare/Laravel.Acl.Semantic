<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->date('nasc')->nullable();
            $table->integer('sex')->nullable();
            $table->string('tel')->nullable();
            $table->string('cel')->nullable();
            $table->string('endereco')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('nasc');
            $table->dropColumn('sex');
            $table->dropColumn('tel');
            $table->dropColumn('cel');
            $table->dropColumn('endereco');
        });
    }
}
