<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */
Route::get('/pdf', 'UtilidadeController@gerarPdf');
Route::get('/excel', 'UtilidadeController@gerarExcel');
Route::get('/mail', 'UtilidadeController@gerarMail');
Route::get('/mailAnexo', 'UtilidadeController@gerarMailAnexo');
Route::get('/criptografia', 'UtilidadeController@gerarCriptografia');
Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
    Route::post('/home', 'HomeController@createPost');
    Route::get('/cidade/{id}', 'ProfileController@citys');

    Route::group(['middleware' =>'web'], function(){
        Route::get('/profile', 'HomeController@profile');
        Route::post('/profile', 'ProfileController@profile');

    });
    //Route::post('/profile/avatar', 'ProfileController@avatar');
});

Route::get('/post/{id}/update', 'HomeController@update');
Route::get('/roles-permission', 'HomeController@rolesPermissions');
